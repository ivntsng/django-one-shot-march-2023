from django.db import models
from django.conf import settings


# Create your models here.
class TodoList(models.Model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    def task_count(self):
        return self.items.count()


class TodoItem(models.Model):
    task = models.CharField(max_length=100)
    due_date = models.DateTimeField(
        null=True,
        blank=True,
    )
    is_completed = models.BooleanField(default=False)

    list = models.ForeignKey(
        TodoList,
        related_name="items",
        on_delete=models.CASCADE,
        default=None,
    )

    def __str__(self):
        return self.task
